﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_7_7
{
    interface IVisitor
    {
        double Visit(DVD DVDItem);
        double Visit(VHS VHSItem);
        double Visit(Book BookItem);
    }
}
