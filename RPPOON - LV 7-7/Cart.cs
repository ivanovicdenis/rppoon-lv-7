﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_7_7
{
    class Cart
    {
        private List<IItem> items;
        public Cart()
        {
            items = new List<IItem>();
        }
        public Cart(List<IItem> items)
        {
            this.items = items;
        }
        public void AddItem(IItem item)
        {
            items.Add(item);
        }
        public void RemoveItem(IItem item)
        {
            items.Remove(item);
        }
        public double Accept(IVisitor visitor)
        {
            double totalPrice = 0;
            foreach (IItem item in this.items)
            {
                try
                {
                    totalPrice += item.Accept(visitor);
                }
                catch (System.NullReferenceException error)
                {
                    Console.WriteLine(error);
                }
            }
            return totalPrice;
        }
    }
}
