﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_7_7
{
    class Program
    {
        static void Main(string[] args)
        {
            IVisitor buyVisitor = new BuyVisitor();
            IVisitor rentVisitor = new RentVisitor();
            List<IItem> items = new List<IItem>();
            items.Add(new Book("Ples zmajeva", 19.98));
            items.Add(new VHS("Ples zmajeva", 10.98));
            items.Add(new DVD("Ples zmajeva", DVDType.MOVIE, 19.98));
            items.Add(new DVD("SolidWorks Pro 2020", DVDType.SOFTWARE, 199.98));

            Cart cart = new Cart(items);
            Console.WriteLine("Ukupna cijena čitave korpe za kupnju: "+ cart.Accept(buyVisitor));
            Console.WriteLine("Ukupna cijena čitave korpe za iznajmljivanje: " + cart.Accept(rentVisitor));

        }
    }
}
