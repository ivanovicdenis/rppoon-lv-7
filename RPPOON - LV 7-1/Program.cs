﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_7_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int sequenceSize = 10;
            NumberSequence numberSequence = new NumberSequence(sequenceSize);
            Random random = new Random();

            int maximum = 100;
            int minimum = 0;
            for (int i =0; i< sequenceSize; i++)
            {
                double randomNumber = random.NextDouble() * (maximum - minimum) + minimum;
                numberSequence.InsertAt(i, Math.Round(randomNumber, 2));
            }

            List<SortStrategy> sortStrategies = new List<SortStrategy>();
            sortStrategies.Add(new BubbleSort());
            sortStrategies.Add(new CombSort());
            sortStrategies.Add(new SequentialSort());

            foreach (SortStrategy strategy in sortStrategies)
            {
                numberSequence.SetSortStrategy(strategy);
                numberSequence.Sort();
                Console.WriteLine("Niz sortiran " +strategy.GetType().Name+ " sortiranjem" );
                Console.WriteLine(numberSequence.ToString());
            }

        }
    }
}
