﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_7_1
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {
            double temp = 0;

            for (int write = 0; write < array.Length; write++)
            {
                for (int sort = 0; sort < array.Length - 1; sort++)
                {
                    if (array[sort] > array[sort + 1])
                    {
                        temp = array[sort + 1];
                        array[sort + 1] = array[sort];
                        array[sort] = temp;
                    }
                }
            }
        }
    }
}
