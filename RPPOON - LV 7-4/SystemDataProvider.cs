﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_7_4
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (previousCPULoad == 0 && currentLoad != 0)
            {
                this.Notify();
                this.previousCPULoad = currentLoad;
                return currentLoad;
            }
            else if (previousCPULoad == 0 && currentLoad == 0)
            {
                this.previousCPULoad = currentLoad;
                return currentLoad;
            }
            else if ((Math.Abs(currentLoad - previousCPULoad) / previousCPULoad * 100) > 10)
            {
                this.Notify();
                this.previousCPULoad = currentLoad;
                return currentLoad;
            }
            else
            {
                this.previousCPULoad = currentLoad;
                return currentLoad;
            }
        }
        public float GetAvailableRAM()
        {
            float currentAvailable = this.AvailableRAM;
            if (previousRAMAvailable==0 && currentAvailable != 0) 
            {
                this.Notify();
                this.previousRAMAvailable = currentAvailable;
                return currentAvailable;
            }
            else if (previousRAMAvailable == 0 && currentAvailable == 0)
            {
                this.previousRAMAvailable = currentAvailable;
                return currentAvailable;
            }
            else if((Math.Abs(currentAvailable - previousRAMAvailable) / previousRAMAvailable * 100) > 10)
            {
                this.Notify();
                this.previousRAMAvailable = currentAvailable;
                return currentAvailable;
            }
            else
            {
                this.previousRAMAvailable = currentAvailable;
                return currentAvailable;
            }
            
        }
    }
}
