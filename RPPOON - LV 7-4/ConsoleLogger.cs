﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_7_4
{
    class ConsoleLogger : Logger
    {
        public void Log(SimpleSystemDataProvider provider)
        {
            Console.WriteLine("Zauzeće procesora: "+ provider.CPULoad);
            Console.WriteLine("Količina slobodne radne memorije: "+provider.AvailableRAM);
        }
    }
}
