﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_7_3
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (currentLoad != this.previousCPULoad)
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }
        public float GetAvailableRAM()
        {
            float currentAvailable = this.AvailableRAM;
            if (currentAvailable != this.previousRAMAvailable)
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentAvailable;
            return currentAvailable;
        }
    }
}
