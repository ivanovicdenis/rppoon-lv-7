﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON___LV_7_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger consoleLogger = new ConsoleLogger();
            Logger fileLogger = new FileLogger("log.txt");
            SystemDataProvider simpleSystemDataProvider = new SystemDataProvider();
            simpleSystemDataProvider.Attach(consoleLogger);
            simpleSystemDataProvider.Attach(fileLogger);

            while (true)
            {
                simpleSystemDataProvider.GetCPULoad();
                simpleSystemDataProvider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);

            }
        }
    }
}
