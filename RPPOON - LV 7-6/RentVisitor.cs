﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace RPPOON___LV_7_6
{
    class RentVisitor: IVisitor
    {
        private const double DVDTax = 0.18;
        private const double VHSTax = 0.10;
        private const double BookTax = 0.24;
        public double Visit(DVD DVDItem)
        {
            if (DVDItem.Type == DVDType.SOFTWARE)
            {
                return Double.NaN;
            }
            else
            {
                return (DVDItem.Price * 10 / 100) * (1 + DVDTax);
            }
        }
        public double Visit(VHS VHSItem)
        {
            return (VHSItem.Price * 10 / 100) * (1 + VHSTax);
        }
        public double Visit(Book BookItem)
        {
            return (BookItem.Price * 10 / 100) * (1 + BookTax);
        }
    }
}
