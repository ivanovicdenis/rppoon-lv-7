﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_7_2
{
    class SequentialSearch : SearchStrategy
    {
        public override int Search(double[] array, double searchNumber)
        {
            int temp = 0;
            foreach(double number in array)
            {
                if (number == searchNumber)
                {
                    return temp;
                }
                temp++;
            }
            return -1;
        }
    }
}