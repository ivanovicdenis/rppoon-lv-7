﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_7_2
{
    abstract class SearchStrategy
    {
        public abstract int Search(double[] array, double searchNumber);
    }
}
