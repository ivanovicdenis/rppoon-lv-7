﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_7_2
{
    class Program
    {

        static void Main(string[] args)
        {
            //Generiranje niza brojeva
            int sequenceSize = 10;
            NumberSequence numberSequence = new NumberSequence(sequenceSize);
            numberSequence.GenerateRandomSequence(sequenceSize, 0, 30);


            double numberToSearch = 8.78;
            numberSequence.InsertAt(5, numberToSearch);

            SearchStrategy searchStrategy = new SequentialSearch();
            numberSequence.SetSearchStrategy(searchStrategy);
            Console.WriteLine("Broj: "+numberToSearch+ " se nalazi na indexu ["+numberSequence.Search(numberToSearch)+"]");

            SortStrategy sortStrategy = new BubbleSort();
            numberSequence.SetSortStrategy(sortStrategy);
            numberSequence.Sort();
            Console.WriteLine("Niz sortiran " + sortStrategy.GetType().Name + " sortiranjem");
            Console.WriteLine(numberSequence.ToString());

            Console.WriteLine("Broj: " + numberToSearch + " se nakon sortiranja nalazi na indexu [" + numberSequence.Search(numberToSearch) + "]");

        }
    }
}
