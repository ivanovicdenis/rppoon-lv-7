﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_7_5
{
    class Program
    {
        static void Main(string[] args)
        {

            IVisitor buyVisitor = new BuyVisitor();
            List<IItem> items = new List<IItem>();
            items.Add(new Book("Ples zmajeva", 19.98));
            items.Add(new VHS("Ples zmajeva", 10.98));
            items.Add(new DVD("Ples zmajeva", DVDType.MOVIE, 19.98));

            foreach (IItem item in items)
            {
                Console.WriteLine("Cijena stavke "+item.GetType().Name+" sa PDV-om iznosi "+ item.Accept(buyVisitor));
            }
        }

    }
}
